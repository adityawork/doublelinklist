package com.year.leap.exercise.doublylinkedlist

import java.lang.StringBuilder

class DoubleLinkedList(firstNodeValue : Int) {
    var head : Node?
        private set

    var tail : Node?
        private set

    var size : Int
        private set

    init {
        val newNode = Node(firstNodeValue)
        head = newNode
        tail = newNode
        size = 1
    }

    private fun createList(firstNodeValue: Int) {
        val newNode = Node(firstNodeValue)
        newNode.next = null
        newNode.prev = null
        head = newNode
        tail = newNode
        size++
    }

    private fun resetList() {
        head = null
        tail = null
        size = 0
    }

    private fun isListEmpty() : Boolean = size == 0

    fun addToStart(value: Int) {
        when {
            // Create 1st node on list empty
            isListEmpty() -> createList(firstNodeValue = value)
            // List has node(s)
            else -> {
                val newNode = Node(value)
                newNode.prev = null
                newNode.next = head
                head?.prev = newNode
                head = newNode
                size++
            }
        }
    }

    fun addToPosition(value: Int, position: Int) {
        when {
            // Empty list
            size == 0 -> createList(firstNodeValue = value)
            // Position is more than the size, add the node at the end
            position > size -> addToEnd(value)
            // Position less than 1, add the node in the beginning
            position < 1 -> addToStart(value)
            // Add the node at the specified position
            else -> {
                var currentPosition = 1
                var positionNode = head
                while (currentPosition < position) {
                    positionNode = positionNode?.next
                    currentPosition++
                }
                val previousNode = positionNode?.prev
                val newNode = Node(value)

                // Updating links
                previousNode?.next = newNode
                newNode.prev = previousNode
                newNode.next = positionNode
                positionNode?.prev = newNode
                size++
            }
        }
    }

    fun addToEnd(value: Int) {
        when {
            // Create 1st node on list empty
            isListEmpty() -> createList(firstNodeValue = value)
            // List has node(s)
            else -> {
                val newNode = Node(value)
                val currentTail = tail
                newNode.next = null
                newNode.prev = tail
                currentTail?.next = newNode
                tail = newNode
                size++
            }
        }
    }

    fun removeHead() {
        when {
            // List is already empty
            isListEmpty() -> {}
            // List only has one node
            size == 1 -> resetList()
            else -> {
                val headNode = head
                val nextNode = headNode?.next
                head = nextNode
                head?.prev = null
                size--
            }
        }
    }

    fun remove(position: Int) {
        when {
            // List is already empty
            isListEmpty() -> {}
            // List only has one node
            size == 1 -> resetList()
            position == 1 -> removeHead()
            position == size -> removeTail()
            else -> {
                var currentNode = head
                var currentPosition = 1
                while (currentPosition < position) {
                    currentNode = currentNode?.next
                    currentPosition++
                }
                val previousNode = currentNode?.prev
                val nextNode = currentNode?.next
                previousNode?.next = nextNode
                nextNode?.prev = previousNode
                size--
            }
        }
    }

    fun removeTail() {
        when {
            // List is already empty
            isListEmpty() -> {}
            // List only has one node
            size == 1 -> resetList()
            else -> {
                val tailNode = tail
                val previousNode = tailNode?.prev
                tail = previousNode
                tail?.next = null
                size--
            }
        }
    }

    fun traverseForward() : String {
        val list = StringBuilder()
        when {
            // List is empty
            isListEmpty() -> list.append("Empty")
            // Traverse list
            else -> {
                var currentNode = head
                do {
                    list.append("-${currentNode?.value}-")
                    currentNode = currentNode?.next
                } while (currentNode != null)
            }
        }
        return list.toString()
    }

    fun traverseBackward() : String {
        val list = StringBuilder()
        when {
            // List is empty
            isListEmpty() -> list.append("Empty")
            // Traverse list
            else -> {
                var currentNode = tail
                do {
                    list.append("-${currentNode?.value}-")
                    currentNode = currentNode?.prev
                } while (currentNode != null)
            }
        }
        return list.toString()
    }
}