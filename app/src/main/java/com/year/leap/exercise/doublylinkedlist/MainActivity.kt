package com.year.leap.exercise.doublylinkedlist

import android.support.v7.app.AppCompatActivity
import android.os.Bundle

class MainActivity : AppCompatActivity(){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onStart() {
        super.onStart()

        val doubleLinkedList = DoubleLinkedList(firstNodeValue = 4)
        doubleLinkedList.addToStart(5)
        doubleLinkedList.addToStart(7)
        doubleLinkedList.addToEnd(8)
        doubleLinkedList.removeHead()
        doubleLinkedList.addToEnd(9)
        doubleLinkedList.addToPosition(0, -1)
        doubleLinkedList.addToPosition(100, 12)
        doubleLinkedList.removeTail()
        doubleLinkedList.addToPosition(13, 3)
        doubleLinkedList.remove(2)
        println("HEAD -> ${doubleLinkedList.head?.value}, TAIL -> ${doubleLinkedList.tail?.value}")
        println(doubleLinkedList.traverseForward())
        println(doubleLinkedList.traverseBackward())
    }
}
