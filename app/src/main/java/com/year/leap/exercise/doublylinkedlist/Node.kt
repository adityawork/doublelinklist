package com.year.leap.exercise.doublylinkedlist

class Node (val value : Int) {
    var prev : Node? = null
    var next : Node? = null
}